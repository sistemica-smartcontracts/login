# login

Provide a small service which generates a JWT after successful login via ethereum account signature as described in https://hackernoon.com/never-use-passwords-again-with-ethereum-and-metamask-b61c7e409f0d. Service implemented in Go